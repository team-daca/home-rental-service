package com.teamdaca.houserentalsystem.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.teamdaca.houserentalsystem.services.UserService;
import com.teamdaca.houserentalsystem.services.UserServiceImp;

@Configuration
public class Config implements WebMvcConfigurer{
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
	    registry
	      .addResourceHandler("/files/**")
	      .addResourceLocations("file:/opt/files/");
	 }
	
}
