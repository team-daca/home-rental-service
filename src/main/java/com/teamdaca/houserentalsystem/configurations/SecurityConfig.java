package com.teamdaca.houserentalsystem.configurations;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.teamdaca.houserentalsystem.services.UserService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Autowired
	private UserService userDetailsService;
	
	
	  @Override
	  protected void configure(AuthenticationManagerBuilder auth)
	      throws Exception {
		  auth.userDetailsService(userDetailsService)
	          .passwordEncoder(bCryptPasswordEncoder);
	  } 
	  
	 @Override
	  protected void configure(HttpSecurity http) throws Exception {
		 
		 http.authorizeRequests()
	     .antMatchers("/").permitAll()
		     .antMatchers("/login", "/searchhomes").permitAll()
		     .antMatchers("/register","/viewhome/**").permitAll()
		     .antMatchers("/index").hasAnyAuthority("TENANT","LANDLORD")
		     .antMatchers("/renthome/**","/addhomereview").hasAnyAuthority("TENANT")
		     .antMatchers("/addhome","/edithome","/landlordhomes","/deletehome","/acceptrent").hasAuthority("LANDLORD")
		     .antMatchers("/actuator/**").permitAll()
		     .anyRequest().authenticated()
		     .and()
		     	.formLogin()
		     			.loginPage("/login")
		     			.failureUrl("/login?error=true")
		     			.defaultSuccessUrl("/index")
		     .and()
		     	.logout()
		     			.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
		     			.logoutSuccessUrl("/login")
		     			.and()
				     	.exceptionHandling()
				     	.accessDeniedPage("/access-denied")
				     	
				     	;
	  }
	 
	 @Override
	 public void configure(WebSecurity webSecurity) throws Exception {
		 
		 webSecurity.ignoring()
		 			.antMatchers("/resources/**","/static/**","/css/**","/js/**","/img/**","/fonts/**");
		 
	 }
	 
	 
}
