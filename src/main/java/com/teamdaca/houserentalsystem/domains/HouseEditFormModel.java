package com.teamdaca.houserentalsystem.domains;

import java.util.List;

import lombok.Data;

@Data
public class HouseEditFormModel {
	private List<House> houses;

	public HouseEditFormModel(List<House> houses) {
		super();
		this.houses = houses;
	}
	
}
