package com.teamdaca.houserentalsystem.domains;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;


import lombok.Data;

@PrimaryKeyJoinColumn(name = "ID")
@Entity
@Data
public class Admin extends User {

}
