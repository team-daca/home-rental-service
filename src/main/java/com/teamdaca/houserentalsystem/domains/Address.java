package com.teamdaca.houserentalsystem.domains;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.*;

@Data
@Entity
public class Address {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long ID;
	
	String city;
	@NotNull(message = "Sub City can not be null")
	String subCity;
	String latitude;
	String longitude;
	String houseNumber;
	@NotNull(message = "Area can not be null")
	String arround;

}

