package com.teamdaca.houserentalsystem.domains;

import java.util.Date;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
public class Rent {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long ID;
	@OneToOne()
	private House house;

	@ManyToOne
	private Tenant tenant;

	@Temporal(TemporalType.DATE)
	private Date _from;
	
	@Temporal(TemporalType.DATE)
	private Date _to;
	
	
}
