package com.teamdaca.houserentalsystem.domains;
import javax.persistence.*;

import lombok.Data;

@Data
@Entity
public class HouseRating {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long ID;
	@ManyToOne
	private House house;
	private int rate;
	private String message;
	@ManyToOne
	private Tenant tenant;

}
