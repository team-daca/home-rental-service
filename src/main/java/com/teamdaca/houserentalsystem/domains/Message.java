package com.teamdaca.houserentalsystem.domains;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
public class Message {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long ID;
	@ManyToOne
	private User sender;
	@ManyToOne
	private User receiver;
	private String content;
	private Date date;

}
