package com.teamdaca.houserentalsystem.domains;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;


import lombok.Data;

@PrimaryKeyJoinColumn(name = "ID")
@Entity
@Data
public class Tenant extends User{
	@OneToMany(orphanRemoval=true)
	private List<HouseRating> houseRating = new ArrayList<HouseRating>();
	@OneToMany
	private List<LandlordRating> landlordRating = new ArrayList<LandlordRating>();
	
	

}
