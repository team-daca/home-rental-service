package com.teamdaca.houserentalsystem.domains;

import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.*;

import lombok.*;

@Data
@Entity
public class House {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long ID;

	@Min(value = 0L, message = "The value must be positive")
	@NotNull(message = "Rooms can not be empty")
	private Integer rooms;

	@Min(value = 0L, message = "The value must be positive")
	@NotNull(message = "Price can not be empty")
	private Double price;

	@OneToOne(orphanRemoval = true)
	private Address address;

	@NotBlank(message = "House type is required")
	private String type;

	@Size(min = 10, message = "Description Should be at least 10 chars")
	private String description;

	@OneToMany(orphanRemoval = true)
	private List<HouseRating> ratings;

	@ManyToOne
	private Landlord owner;
	
	private String image;

	public boolean equals(Object o) {
		if (o instanceof House) {
			if (((House) o).ID == this.ID) {
				return true;
			}
		}
		return false;
	}

	public int getAverageRating() {
		if (ratings == null)
			return 0;

		else if (ratings.size() == 0)
			return 0;

		int total = 0;
		for (HouseRating r : ratings)
			total += r.getRate();

		return total / ratings.size();

	}
}
