package com.teamdaca.houserentalsystem.domains;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
public class LandlordRating {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long ID;
	@ManyToOne
	private Landlord landlord;
	private String message;
	private int rate;
	@ManyToOne
	private Tenant tenant;

}
