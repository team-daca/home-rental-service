package com.teamdaca.houserentalsystem.domains;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;


import lombok.Data;

@PrimaryKeyJoinColumn(name = "ID")
@Entity
@Data
public class Landlord extends User{
	@OneToMany
	private List<LandlordRating> landlordRating = new ArrayList<LandlordRating>();

	@OneToMany(orphanRemoval=true)
	private List<House> house = new ArrayList<House>();

}
