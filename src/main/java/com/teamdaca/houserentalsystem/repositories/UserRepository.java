package com.teamdaca.houserentalsystem.repositories;
import org.springframework.data.repository.CrudRepository;

import com.teamdaca.houserentalsystem.domains.House;
import com.teamdaca.houserentalsystem.domains.User;

public interface UserRepository extends CrudRepository<User, Long>{
	User findByUsername(String username);
}
