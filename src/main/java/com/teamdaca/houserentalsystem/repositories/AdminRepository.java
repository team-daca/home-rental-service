package com.teamdaca.houserentalsystem.repositories;

import org.springframework.data.repository.CrudRepository;

import com.teamdaca.houserentalsystem.domains.*;


public interface AdminRepository extends CrudRepository<Admin, Long>{

}

