package com.teamdaca.houserentalsystem.repositories;

import org.springframework.data.repository.CrudRepository;

import com.teamdaca.houserentalsystem.domains.*;


public interface TenantRepository extends CrudRepository<Tenant, Long>{

}
