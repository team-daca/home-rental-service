package com.teamdaca.houserentalsystem.repositories;

import org.springframework.data.repository.CrudRepository;

import com.teamdaca.houserentalsystem.domains.House;

public interface HouseRepository extends CrudRepository<House, Long>{
	Iterable<House> findAllByDescription(String keyword);
	Iterable<House> findByDescriptionContains(String keyword);
}
