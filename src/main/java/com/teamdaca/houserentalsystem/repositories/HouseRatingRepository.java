package com.teamdaca.houserentalsystem.repositories;

import org.springframework.data.repository.CrudRepository;

import com.teamdaca.houserentalsystem.domains.*;

public interface HouseRatingRepository extends CrudRepository<HouseRating, Long>{

	Iterable<HouseRating> findAllByHouse(House house);
}

