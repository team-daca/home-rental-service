package com.teamdaca.houserentalsystem.repositories;

import org.springframework.data.repository.CrudRepository;

import com.teamdaca.houserentalsystem.domains.*;


public interface RoleRepository extends CrudRepository<Role, Long>{
	 Role findByName(String role);
}
