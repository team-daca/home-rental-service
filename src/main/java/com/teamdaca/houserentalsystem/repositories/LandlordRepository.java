package com.teamdaca.houserentalsystem.repositories;

import org.springframework.data.repository.CrudRepository;

import com.teamdaca.houserentalsystem.domains.House;
import com.teamdaca.houserentalsystem.domains.Landlord;



public interface LandlordRepository extends CrudRepository<Landlord, Long>{

}