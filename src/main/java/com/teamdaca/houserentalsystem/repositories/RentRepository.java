package com.teamdaca.houserentalsystem.repositories;

import org.springframework.data.repository.CrudRepository;

import com.teamdaca.houserentalsystem.domains.*;


public interface RentRepository extends CrudRepository<Rent, Long>{
	Iterable<Rent> findAllByHouse(House house);
}
