package com.teamdaca.houserentalsystem.repositories;

import org.springframework.data.repository.CrudRepository;

import com.teamdaca.houserentalsystem.domains.*;


public interface MessageRepository extends CrudRepository<Message, Long>{

}
