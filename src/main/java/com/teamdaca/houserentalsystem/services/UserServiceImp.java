package com.teamdaca.houserentalsystem.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.teamdaca.houserentalsystem.domains.User;
import com.teamdaca.houserentalsystem.repositories.RoleRepository;
import com.teamdaca.houserentalsystem.repositories.UserRepository;

@Service
public class UserServiceImp implements UserService{
	
	private UserRepository userRepository;
   private RoleRepository roleRepository;
   private BCryptPasswordEncoder bCryptPasswordEncoder;

   @Autowired
   public UserServiceImp(UserRepository userRepository,
   		RoleRepository roleRepository,
                      BCryptPasswordEncoder bCryptPasswordEncoder) {
       this.userRepository = userRepository;
       this.roleRepository = roleRepository;
       this.bCryptPasswordEncoder = bCryptPasswordEncoder;
   }

 
   
   public void saveUser(User user) {
       user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

       userRepository.save(user);
   }

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		User user = userRepository.findByUsername(username);
		
		if(user != null) {
			return user;
		}
		throw new UsernameNotFoundException("User '" + username + "' not found");
	}

	@Override
	public User findUserByUsername(String username) {
		return userRepository.findByUsername(username);
		
	}
}
