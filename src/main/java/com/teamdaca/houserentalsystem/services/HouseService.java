package com.teamdaca.houserentalsystem.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teamdaca.houserentalsystem.domains.Address;
import com.teamdaca.houserentalsystem.domains.House;
import com.teamdaca.houserentalsystem.domains.Role;
import com.teamdaca.houserentalsystem.repositories.AddressRepository;
import com.teamdaca.houserentalsystem.repositories.HouseRepository;

public interface HouseService{
public House save(House house);
	
	public Iterable<House> saveAll(Iterable<House> houses);

	Optional<House> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<House> findAll();

	Iterable<House> findAllById(Iterable<Long> ids);
	Iterable<House> findByDescriptionContains(String keyword);

	long count();
	
	void deleteById(Long id);
	
	void delete(House house);
	
	void deleteAll(Iterable<House> houses);

	void deleteAll();
}


@Service
 class HouseServiceImple implements HouseService {
	 
	 @Autowired
	 HouseRepository houserRepository;
	 
	 @Autowired
	 AddressRepository addressRepository;

	@Override
	public House save(House house) {
		// TODO Auto-generated method stub
		Address address = addressRepository.save(house.getAddress());
		house.setAddress(address);
		return houserRepository.save(house);
	}

	@Override
	public Iterable<House> saveAll(Iterable<House> houses) {
		houses.forEach(h -> h.setAddress(addressRepository.save(h.getAddress())));
		// TODO Auto-generated method stub
		return houserRepository.saveAll(houses);
	}

	@Override
	public Optional<House> findById(Long id) {
		// TODO Auto-generated method stub
		return houserRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<House> findAll() {
		// TODO Auto-generated method stub
		return houserRepository.findAll();
	}

	@Override
	public Iterable<House> findAllById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return houserRepository.findAllById(ids);
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return houserRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		houserRepository.deleteById(id);
		
	}

	@Override
	public void delete(House house) {
		// TODO Auto-generated method stub
		houserRepository.delete(house);
	}

	@Override
	public void deleteAll(Iterable<House> houses) {
		// TODO Auto-generated method stub
		houserRepository.deleteAll(houses);
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		houserRepository.deleteAll();
		
	}

	@Override
	public Iterable<House> findByDescriptionContains(String keyword) {
		// TODO Auto-generated method stub
		return houserRepository.findByDescriptionContains(keyword);
	}

}
