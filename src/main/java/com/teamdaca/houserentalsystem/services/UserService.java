package com.teamdaca.houserentalsystem.services;



import org.springframework.security.core.userdetails.UserDetailsService;

import com.teamdaca.houserentalsystem.domains.User;;

public interface UserService extends UserDetailsService {

	User findUserByUsername(String username);
	void saveUser(User user);
	
}

