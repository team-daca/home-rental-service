package com.teamdaca.houserentalsystem.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.teamdaca.houserentalsystem.domains.Tenant;
import com.teamdaca.houserentalsystem.repositories.TenantRepository;

public interface TenantService {
	
	public Tenant save(Tenant tenant);
	
	public Iterable<Tenant> saveAll(Iterable<Tenant> tenants);

	Optional<Tenant> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<Tenant> findAll();

	Iterable<Tenant> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(Tenant tenant);
	
	void deleteAll(Iterable<Tenant> tenants);

	void deleteAll();
}

@Service
class TenantServiceImp implements TenantService{
	
	@Autowired
	TenantRepository tenantRepository;
	

	
	@Override
	public Tenant save(Tenant tenant) {
		
		return tenantRepository.save(tenant);
	}

	@Override
	public Iterable<Tenant> saveAll(Iterable<Tenant> tenants) {
		return tenantRepository.saveAll(tenants);
	}

	@Override
	public Optional<Tenant> findById(Long id) {
		return tenantRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		return tenantRepository.existsById(id);
	}

	@Override
	public Iterable<Tenant> findAll() {
		return tenantRepository.findAll();
	}

	@Override
	public Iterable<Tenant> findAllById(Iterable<Long> ids) {
		return tenantRepository.findAllById(ids);
	}

	@Override
	public long count() {
		return tenantRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		tenantRepository.deleteById(id);
	}

	@Override
	public void delete(Tenant tenant) {
		tenantRepository.delete(tenant);
	}

	@Override
	public void deleteAll(Iterable<Tenant> tenants) {
		tenantRepository.deleteAll(tenants);
	}

	@Override
	public void deleteAll() {
		tenantRepository.deleteAll();
	}

	
}