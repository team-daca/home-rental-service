package com.teamdaca.houserentalsystem.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teamdaca.houserentalsystem.domains.Address;
import com.teamdaca.houserentalsystem.domains.House;
import com.teamdaca.houserentalsystem.domains.HouseRating;
import com.teamdaca.houserentalsystem.domains.Role;
import com.teamdaca.houserentalsystem.repositories.AddressRepository;
import com.teamdaca.houserentalsystem.repositories.HouseRatingRepository;

public interface HouseRatingService{
public HouseRating save(HouseRating rating);
	
	public Iterable<HouseRating> saveAll(Iterable<HouseRating> ratings);

	Optional<HouseRating> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<HouseRating> findAll();

	Iterable<HouseRating> findAllById(Iterable<Long> ids);

	long count();

	void deleteById(Long id);
	Iterable<HouseRating> findAllByHouse(House house);
	
	void delete(HouseRating rating);
	
	void deleteAll(Iterable<HouseRating> ratings);

	void deleteAll();
}


@Service
 class HouseRatingServiceImple implements HouseRatingService {
	 
	 @Autowired
	 HouseRatingRepository houseRatingRepository;
	 

	@Override
	public HouseRating save(HouseRating rating) {
		
		return houseRatingRepository.save(rating);
	}

	@Override
	public Iterable<HouseRating> saveAll(Iterable<HouseRating> ratings) {
		
		// TODO Auto-generated method stub
		return houseRatingRepository.saveAll(ratings);
	}

	@Override
	public Optional<HouseRating> findById(Long id) {
		// TODO Auto-generated method stub
		return houseRatingRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<HouseRating> findAll() {
		// TODO Auto-generated method stub
		return houseRatingRepository.findAll();
	}

	@Override
	public Iterable<HouseRating> findAllById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return houseRatingRepository.findAllById(ids);
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return houseRatingRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		houseRatingRepository.deleteById(id);
		
	}

	@Override
	public void delete(HouseRating rating) {
		// TODO Auto-generated method stub
		houseRatingRepository.delete(rating);
	}

	@Override
	public void deleteAll(Iterable<HouseRating> ratings) {
		// TODO Auto-generated method stub
		houseRatingRepository.deleteAll(ratings);
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		houseRatingRepository.deleteAll();
		
	}

	@Override
	public 	Iterable<HouseRating> findAllByHouse(House house) {
		// TODO Auto-generated method stub
		return houseRatingRepository.findAllByHouse(house);
		
	}



}
