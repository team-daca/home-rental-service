package com.teamdaca.houserentalsystem.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.teamdaca.houserentalsystem.domains.Landlord;
import com.teamdaca.houserentalsystem.repositories.LandlordRepository;

public interface LandlordService {
	
	public Landlord save(Landlord Landlord);
	
	public Iterable<Landlord> saveAll(Iterable<Landlord> Landlords);

	Optional<Landlord> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<Landlord> findAll();

	Iterable<Landlord> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(Landlord Landlord);
	
	void deleteAll(Iterable<Landlord> Landlords);

	void deleteAll();
}

@Service
class LandlordServiceImp implements LandlordService{
	
	@Autowired
	LandlordRepository landlordRepository;
	

	
	@Override
	public Landlord save(Landlord landlord) {
		
		return landlordRepository.save(landlord);
	}

	@Override
	public Iterable<Landlord> saveAll(Iterable<Landlord> Landlords) {
		return landlordRepository.saveAll(Landlords);
	}

	@Override
	public Optional<Landlord> findById(Long id) {
		return landlordRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		return landlordRepository.existsById(id);
	}

	@Override
	public Iterable<Landlord> findAll() {
		return landlordRepository.findAll();
	}

	@Override
	public Iterable<Landlord> findAllById(Iterable<Long> ids) {
		return landlordRepository.findAllById(ids);
	}

	@Override
	public long count() {
		return landlordRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		landlordRepository.deleteById(id);
	}

	@Override
	public void delete(Landlord Landlord) {
		landlordRepository.delete(Landlord);
	}

	@Override
	public void deleteAll(Iterable<Landlord> Landlords) {
		landlordRepository.deleteAll(Landlords);
	}

	@Override
	public void deleteAll() {
		landlordRepository.deleteAll();
	}

	
}