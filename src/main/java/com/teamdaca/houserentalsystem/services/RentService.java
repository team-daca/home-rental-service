package com.teamdaca.houserentalsystem.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teamdaca.houserentalsystem.domains.Address;
import com.teamdaca.houserentalsystem.domains.House;
import com.teamdaca.houserentalsystem.domains.Rent;
import com.teamdaca.houserentalsystem.domains.Role;
import com.teamdaca.houserentalsystem.repositories.AddressRepository;
import com.teamdaca.houserentalsystem.repositories.RentRepository;

public interface RentService{
public Rent save(Rent rent);
	
	public Iterable<Rent> saveAll(Iterable<Rent> rents);

	Optional<Rent> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<Rent> findAll();

	Iterable<Rent> findAllById(Iterable<Long> ids);
	
	Iterable<Rent> findAllByHouse(House house);

	long count();
	
	void deleteById(Long id);
	
	void delete(Rent rent);
	
	void deleteAll(Iterable<Rent> rentes);

	void deleteAll();
}


@Service
 class RentServiceImple implements RentService {
	 
	 @Autowired
	 RentRepository rentRepository;
	 

	@Override
	public Rent save(Rent rent) {
		// TODO Auto-generated method stub

		return rentRepository.save(rent);
	}

	@Override
	public Iterable<Rent> saveAll(Iterable<Rent> rents) {
		
		// TODO Auto-generated method stub
		return rentRepository.saveAll(rents);
	}

	@Override
	public Optional<Rent> findById(Long id) {
		// TODO Auto-generated method stub
		return rentRepository.findById(id);
	}

	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<Rent> findAll() {
		// TODO Auto-generated method stub
		return rentRepository.findAll();
	}

	@Override
	public Iterable<Rent> findAllById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return rentRepository.findAllById(ids);
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return rentRepository.count();
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		rentRepository.deleteById(id);
		
	}

	@Override
	public void delete(Rent house) {
		// TODO Auto-generated method stub
		rentRepository.delete(house);
	}

	@Override
	public void deleteAll(Iterable<Rent> houses) {
		// TODO Auto-generated method stub
		rentRepository.deleteAll(houses);
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		rentRepository.deleteAll();
		
	}

	@Override
	public Iterable<Rent> findAllByHouse(House house) {
		// TODO Auto-generated method stub
		return rentRepository.findAllByHouse(house);
	}

	

}
