package com.teamdaca.houserentalsystem.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.*;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Paths;
import com.teamdaca.houserentalsystem.domains.House;
import com.teamdaca.houserentalsystem.domains.HouseEditFormModel;
import com.teamdaca.houserentalsystem.domains.HouseRating;
import com.teamdaca.houserentalsystem.domains.Landlord;
import com.teamdaca.houserentalsystem.domains.Rent;
import com.teamdaca.houserentalsystem.domains.Tenant;
import com.teamdaca.houserentalsystem.domains.User;
import com.teamdaca.houserentalsystem.services.HouseRatingService;
import com.teamdaca.houserentalsystem.services.HouseService;
import com.teamdaca.houserentalsystem.services.LandlordService;
import com.teamdaca.houserentalsystem.services.RentService;
import com.teamdaca.houserentalsystem.services.TenantService;
import com.teamdaca.houserentalsystem.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class HouseController {

	@Autowired
	HouseService houseService;

	@Autowired
	LandlordService landlordService;

	@Autowired
	TenantService tenantService;

	@Autowired
	RentService rentService;

	@Autowired
	UserService userService;
	
	@Autowired
	HouseRatingService houseRatingService;

	@ModelAttribute(name = "NewHouse")
	public House newHouseModel() {
		House house = new House();
		return house;
	}

	@ModelAttribute(name = "HomeTypes")
	public String[] homeTypesModle() {
		return new String[] { "One Bedroom", "Two Bedrooms", "Three Bedrooms", "Office", "Warehouse" };
	}

	@ModelAttribute(name = "SubCities")
	public String[] subCitiesModle() {
		return new String[] { "Arada", "Kirkos", "Bole", "Gulelle" };
	}
	
	@ModelAttribute(name = "LoggedInUser")
	public User loggedInUserModle(@AuthenticationPrincipal User user ){
		
		return user;
	}

	@GetMapping("/addhome")
	public String addHome() {
		return "addhome";
	}

	@PostMapping("addhome")
	public String addHomePost(@Valid @ModelAttribute("NewHouse") House newHouse, BindingResult bindingResult,
			@RequestParam("file") MultipartFile file, @AuthenticationPrincipal User user) {

		if (bindingResult.hasErrors()) {
			return "addhome";
		}
		Landlord loggedInUser = landlordService.findById(user.getID()).get();
		newHouse.setOwner(loggedInUser);
		 try (InputStream inputStream = file.getInputStream()) {
             try {
            	 String[] fileNameSplit = file.getOriginalFilename().split("\\.");
            	  System.out.println(file.getOriginalFilename() + "It is th file");
            	 String fileName = Calendar.getInstance().getTimeInMillis() + "." + fileNameSplit[fileNameSplit.length - 1];
				newHouse.setImage(fileName);
            	 Files.copy(inputStream, Paths.get("/opt/files/",fileName), StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				
			}
         } catch (IOException e1) {
			
		}
		House h = houseService.save(newHouse);
		
		loggedInUser.getHouse().add(h);
		landlordService.save(loggedInUser);

		return "redirect:/landlordhomes";
	}

	@GetMapping("/deletehome")
	public String deleteHome(@RequestParam long id, @AuthenticationPrincipal User user) {
		Landlord landlord = landlordService.findById(user.getID()).get();

		int homeIndex = landlord.getHouse().indexOf(houseService.findById(id).get());

		// Check if house is logged in user's
		if (homeIndex != 1) {
			
			List<Rent> relatedRents = (ArrayList) rentService.findAllByHouse(houseService.findById(id).get());
			relatedRents.forEach(r -> {
				landlord.getRent().remove(r);
				Tenant tenant = r.getTenant();
				tenant.getRent().remove(r);
				tenantService.save(tenant);
				
			});
			
			landlord.getHouse().remove(homeIndex);
			landlordService.save(landlord);
			rentService.deleteAll(relatedRents);
			House h = houseService.findById(id).get();
			h.getRatings().forEach(r -> {
				Tenant t = r.getTenant();
				t.getHouseRating().remove(r);
				tenantService.save(t);
			});
			h.getRatings().clear();
			houseService.save(h);
			houseRatingService.deleteAll(houseRatingService.findAllByHouse(h));
			houseService.deleteById(id);
		}

		return "redirect:/landlordhomes";
	}

	@GetMapping("/landlordhomes")
	public String landlordHomes(Model model, @AuthenticationPrincipal User user) {
		Landlord landlord = landlordService.findById(user.getID()).get();

		model.addAttribute("houses", landlord.getHouse());

		return "landlordhomes";
	}

	@GetMapping("/edithome")
	public String editHome(@RequestParam("id") Long id, Model model) {
		Optional<House> houseOpt = houseService.findById(id);

		if (houseOpt.isPresent()) {
			model.addAttribute("EditedHouse", houseOpt.get());
			return "edithome";
		}

		return "redirect:/";

	}

	@PostMapping("/edithome")
	public String editHomePost(@Valid @ModelAttribute("EditedHouse") House house, BindingResult bindingResult,
			@AuthenticationPrincipal User user) {
		if (bindingResult.hasErrors()) {
			return "edithome";
		}

		Landlord landlord = landlordService.findById(user.getID()).get();

		houseService.save(house);
		return "redirect:/landlordhomes";
	}

	@GetMapping("/searchhomes")
	public String searchHomes(@RequestParam String keyword, Model model) {
		ArrayList<House> result = (ArrayList<House>) houseService.findByDescriptionContains(keyword);
		model.addAttribute("searchResultHouses", result);

		return "searchhomes";
	}

	@GetMapping("/rents")
	public String rents(@AuthenticationPrincipal User user, Model model, Authentication authentication) {
		// for landlord
		//must check user role
		
		model.addAttribute("Rents", userService.findUserByUsername(user.getUsername()).getRent());
		
		return "rents";
	}

	@GetMapping("/viewhome/{id}")
	public String viewHome(@PathVariable("id") Long id, Model model) {
		Optional<House> houseOpt = houseService.findById(id);
		List<House> houses = (List<House>) houseService.findAll();

		if (houseOpt.isPresent()) {
			model.addAttribute("Home", houseOpt.get());
			model.addAttribute("SuggestedHomes", houses);
			model.addAttribute("NewReview", new HouseRating());
			return "home";
		}

		return "/";
	}

	@GetMapping("/renthome/{id}")
	public String rentHome(@PathVariable("id") Long id, @AuthenticationPrincipal User user) {
		
		Optional<House> houseOpt = houseService.findById(id);
		if (houseOpt.isPresent()) {
			Tenant tenant = tenantService.findById(user.getID()).get();
			Landlord landlord = houseOpt.get().getOwner();
			Rent rent = new Rent();
			rent.setHouse(houseOpt.get());
			rent.setTenant(tenant);	
			rent = rentService.save(rent);
			tenant.getRent().add(rent);
			landlord.getRent().add(rent);
			
			tenantService.save(tenant);
			landlordService.save(landlord);
		}
		//should be redirected to /rents
		
		
		return "redirect:/rents";
	}
	
	
	@PostMapping("/acceptrent/{id}")
	public String acceptRent(@PathVariable("id") Long id, @RequestParam("from") String _from, @RequestParam("to") String _to) {
		//not working check back
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Rent rent = rentService.findById(id).get();
		try {
			rent.set_from(format.parse(_from));
			rent.set_to(format.parse(_to));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		rentService.save(rent);
		
		return "redirect:/rents";
	}
	
	@PostMapping("/addhomereview/{houseID}")
	public String addHomeReview(@PathVariable("houseID") Long houseID, @RequestParam("message") String message, @RequestParam("rating") int rating, @AuthenticationPrincipal User user) {
		House house = houseService.findById(houseID).get();
		Tenant tenant = tenantService.findById(user.getID()).get();
		HouseRating houseRating = new HouseRating();
		houseRating.setHouse(house);
		houseRating.setMessage(message);
		houseRating.setRate(rating);
		houseRating.setTenant(tenant);
		houseRating = houseRatingService.save(houseRating);
		
		tenant.getHouseRating().add(houseRating);
		house.getRatings().add(houseRating);
		tenantService.save(tenant);
		houseService.save(house);
		
		return "redirect:/viewhome/" + houseID;
		
	}

}
