package com.teamdaca.houserentalsystem.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.teamdaca.houserentalsystem.domains.House;
import com.teamdaca.houserentalsystem.domains.User;
import com.teamdaca.houserentalsystem.services.HouseService;
import com.teamdaca.houserentalsystem.services.UserService;

@Controller
public class IndexController {
	
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private HouseService houseService;
	
	@ModelAttribute(name="houseFeeds")
	public List<House> houseFeedsModel(){
		
		return (List<House>) houseService.findAll();
	}
	
	@ModelAttribute(name = "LoggedInUser")
	public User loggedInUserModle(@AuthenticationPrincipal User user ){
		
		return user;
	}
	@GetMapping("/index")
	public String Index(@AuthenticationPrincipal UserDetails userDetails, Model model) {
		User user = userService.findUserByUsername(userDetails.getUsername());
		model.addAttribute("USER", user);
		
		return "index";
	}
	
	@GetMapping("/")
	public String Home() {
		return "index";
	}
	
	@GetMapping("/access-denied")
	public String AccesDenied() {
		return "access_denied";
	}
	
	
}
