package com.teamdaca.houserentalsystem.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.teamdaca.houserentalsystem.domains.Landlord;
import com.teamdaca.houserentalsystem.domains.Role;
import com.teamdaca.houserentalsystem.domains.Tenant;
import com.teamdaca.houserentalsystem.domains.User;
import com.teamdaca.houserentalsystem.services.LandlordService;
import com.teamdaca.houserentalsystem.services.RoleService;
import com.teamdaca.houserentalsystem.services.TenantService;
import com.teamdaca.houserentalsystem.services.UserService;

@Controller
public class RegistrationController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	LandlordService landlordService;
	
	@Autowired
	BCryptPasswordEncoder bCryptPasswordEncoder;
	

	@Autowired
	TenantService tenantService;
	
	@ModelAttribute(name = "NewUser")
	public User getNewUser() {
		return new User();
	}
	
	@ModelAttribute(name = "roles")
	public List<Role> roleModel(){
		
		List<Role> roles = (List<Role>) roleService.findAll();
		roles = roles.stream().filter(r -> !r.getName().equals("ADMIN")).collect(Collectors.toList());
		return roles;
	}

	@GetMapping("login")
	public String login() {
		return "login";
	}
	
	@GetMapping("register")
	public String showRegistrationForm() {
		return "register";
	}
	
	
	@PostMapping("/register")
	public String register(@Valid @ModelAttribute("NewUser") User NewUser, BindingResult bindingResult) {
		
		if(bindingResult.hasErrors()) {
			
			return "register";
		}
		
		User user = userService.findUserByUsername(NewUser.getUsername());
		if(user != null) {
			bindingResult.rejectValue("username", "error.username", "Try Another User Name");
			return "register";
		}
		
		if(NewUser.getRoles().contains(roleService.findByName("LANDLORD"))) {
			
			Landlord landlord = new Landlord();
			landlord.setFullName(NewUser.getFullName());
			landlord.setUsername(NewUser.getUsername());
			landlord.setRoles(NewUser.getRoles());
			landlord.setPassword(bCryptPasswordEncoder.encode(NewUser.getPassword()));
			landlordService.save(landlord);
		}
		else {
			
			Tenant tenant = new Tenant();
			tenant.setFullName(NewUser.getFullName());
			tenant.setUsername(NewUser.getUsername());
			tenant.setRoles(NewUser.getRoles());
			tenant.setPassword(bCryptPasswordEncoder.encode(NewUser.getPassword()));
			tenantService.save(tenant);
		}
		
		
		return "index";
	}
}
