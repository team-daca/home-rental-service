package com.teamdaca.houserentalsystem;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.teamdaca.houserentalsystem.controllers.RegistrationController;
import com.teamdaca.houserentalsystem.services.LandlordService;
import com.teamdaca.houserentalsystem.services.UserService;
import com.teamdaca.houserentalsystem.services.UserServiceImp;


@RunWith(SpringRunner.class)
@WebMvcTest(RegistrationController.class)
public class RegistrationControllerTest {
	
	@Autowired
	  private MockMvc mockMvc;

	@MockBean	
	  private UserService userService;
	
	@MockBean	
	  private LandlordService rolrService;
	
	@MockBean
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@Test
	  public void testRegisterPage() throws Exception {
	    mockMvc.perform(get("/register"))
	      .andExpect(status().isOk())
	      .andExpect(view().name("register"))
	      .andExpect(content().string(
	          containsString("Register")));  
	  }
	
	@Test
	  public void testLoginPage() throws Exception {
	    mockMvc.perform(get("/login"))
	      .andExpect(status().isOk())
	      .andExpect(view().name("login"));  
	  }
}
