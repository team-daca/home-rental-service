package com.teamdaca.houserentalsystem;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.teamdaca.houserentalsystem.controllers.IndexController;
import com.teamdaca.houserentalsystem.services.UserService;

@RunWith(SpringRunner.class)
@WebMvcTest(IndexController.class)

public class IndexControllerTest {
	@Autowired
	  private MockMvc mockMvc;
	@MockBean	
	  private UserService userService;
	
	@MockBean
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Test
	@WithMockUser(username="testuser", password="testuser", authorities="TENANT")
	  public void testIndexPage() throws Exception {
	    mockMvc.perform(get("/index"))
	      .andExpect(status().isOk())
	      .andExpect(view().name("index"))
	      .andExpect(content().string(
	          containsString("House Rental System")));  
	  }
	
	@Test
	  public void testAccesDeniedPage() throws Exception {
	    mockMvc.perform(get("/access-denied"))
	      .andExpect(status().isOk())
	      .andExpect(view().name("access_denied"))
	      .andExpect(content().string(
	          containsString("You can not access this resource.")));  
	  }
	
	@Test
	  public void testHomePage() throws Exception {
	    mockMvc.perform(get("/"))
	      .andExpect(status().isOk())
	      .andExpect(view().name("index"))
	      .andExpect(content().string(
	          containsString("House Rental System")));  
	  }
}

